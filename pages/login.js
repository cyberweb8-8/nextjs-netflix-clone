import Head from 'next/head';
import Link from 'next/link';
import Image from 'next/image';

import s from '../styles/Login.module.css';

const Login = () => {
  const handleLoginWithEmail = (e) => {
    e.preventDefault();
    console.log('Hi button');
  };
  return (
    <div className={s.container}>
      <Head>
        <title>Netflix Signin</title>
      </Head>
      <header className={s.header}>
        <div className={s.headerWrapper}>
          <Link href="/">
            <a className={s.logoLink}>
              <div className={s.logoWrapper}>
                <Image src="/static/netflix.svg" alt="Netflix logo" width="130px" height="34px" />
              </div>
            </a>
          </Link>
        </div>
      </header>
      <main>
        <div className={s.mainWrapper}>
          <h1 className={s.signinTitle}>Sign In</h1>
          <input type="text" placeholder="Email address" className={s.emailInput} />
          <p className={s.userMsg}>Please enter valid email address.</p>
          <button onClick={handleLoginWithEmail} className={s.loginBtn}>
            Sign In
          </button>
        </div>
      </main>
    </div>
  );
};

export default Login;
