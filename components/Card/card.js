import Image from 'next/image';
import { useState } from 'react';
import s from './card.module.css';

import { motion } from 'framer-motion';
import cls from 'classnames';

const Card = (props) => {
  const { id, size = 'medium', imgUrl = '/static/cliffor.webp' } = props;
  const [imgSrc, setImgSrc] = useState(imgUrl);
  const classMap = {
    large: s.lgItem,
    medium: s.mdItem,
    small: s.smItem,
  };
  const handleOnError = () => {
    // https://unsplash.com/photos/eVqU1HTZL8E?utm_source=unsplash&utm_medium=referral&utm_content=creditShareLink
    setImgSrc('/static/default.webp');
  };
  const scale = id === 0 ? { scaleY: 1.1 } : { scale: 1.1 };
  return (
    <div className={s.container}>
      <motion.div className={cls(s.imgMotionWrapper, classMap[size])} whileHover={{ ...scale }}>
        <Image
          src={imgSrc}
          alt="image"
          layout="fill"
          className={s.cardImg}
          onError={handleOnError}
        />
      </motion.div>
    </div>
  );
};

export default Card;
