import videoData from '../data/videos.json';

export const getCommonVideos = async (url) => {
  const youtube_api_key = process.env.YOUTUBE_API_KEY;

  try {
    const BASE_URL = 'youtube.googleapis.com/youtube/v3';
    const response = await fetch(`https://${BASE_URL}/${url}&maxResults=25&key=${youtube_api_key}`);
    const data = await response.json();
    if (data?.error) {
      console.log('Youtube API error', data.error);
      return [];
    }

    return data.items.map((item) => {
      const id = item.id?.videoId || item.id;
      return {
        title: item.snippet.title,
        imgUrl: item.snippet.thumbnails.high.url,
        id,
      };
    });
  } catch (error) {
    console.log('Someghin went wrong', error);
    return [];
  }
};

export const getVideos = async (searchQuery) => {
  const url = `search?part=snippet&q=${searchQuery}&type=video`;
  return getCommonVideos(url);
};

export const getPopularVideos = async () => {
  const url = `videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&regionCode=US&type=video`;
  return getCommonVideos(url);
};
